package db.dao;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Book {
    private String title;
    private String subtitle;
    private String author;
    private String isbn;
    private Integer issued_in;
}
