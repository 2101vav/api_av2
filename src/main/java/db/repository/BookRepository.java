package db.repository;

import db.dao.Book;

import java.util.List;

public interface BookRepository {
    Book getBookByTitle(String title);
    Book getBookByIsbn(String isbn);
    Book getBookById(Integer id);
    List<Book> getAllBooks();
    void saveBook(Book book);
    void deleteBookById(Integer id);
    void deleteBookByTitle(String title);
    void deleteBookByIsbn(String isbn);
    void updateBook(Book book,String findBook);
    void updateIssued(Book book,Integer issued,Integer year,String isbn);

}
