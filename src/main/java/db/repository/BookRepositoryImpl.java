package db.repository;

import db.config.StatementInit;
import db.dao.Book;
import db.exceptions.BookNotFoundException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/*
* INSERT INTO BOOKS (title,subtitle,author,isbn,issued_in)
values('Java: The Complete Reference, Twelfth Edition 12th Edition',
	'The Definitive Java Programming Guide',
	'Herbert Schildt',
	'9781260463422',
	2022
	)
	* INSERT INTO BOOKS (title,subtitle,author,isbn,issued_in)
values('Modern Java in Action: Lambdas, streams, functional and reactive programming',
	'Manning`s bestselling Java 8 book',
	'Raoul-Gabriel Urma, Mario Fusco',
	'9781617293566',
	2019
	)
*INSERT INTO BOOKS (title,subtitle,author,isbn,issued_in)
values('Optimizing Java: Practical Techniques for Improving JVM Application Performance 1st Edition',
	'This practical guide',
	'Benjamin J Evans',
	'9781492025795',
	2018
	)
*INSERT INTO BOOKS (title,subtitle,author,isbn,issued_in)
values('Core Java for the Impatient 3rd Edition',
	'Clear, Concise Guide to the Core Language and Libraries-Updated through Java 17',
	'Cay S. Horstmann',
	'9780138052102',
	2022
	)
	* INSERT INTO BOOKS (title,subtitle,author,isbn,issued_in)
values('Learning Java: An Introduction to Real-World Programming with Java 6th Edition',
	'Ideal for working programmers new to Java',
	'Daniel Leuck',
	'9781098145538',
	2023
	)
*  */
public class BookRepositoryImpl implements BookRepository{
    @Override
    public Book getBookByTitle(String title) {
                try {
                String sql = "SELECT * FROM BOOKS WHERE title=?";
                PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
                preparedStatement.setString(1, title);

                ResultSet rs = preparedStatement.executeQuery();

                if (rs.next()) {
                    return new Book(rs.getString("title"),
                            rs.getString("subtitle"),
                            rs.getString("author"),
                            rs.getString("isbn"),
                            rs.getInt("issued_in"));
                } else {
                    throw new BookNotFoundException("The Book is not found in DB");
                }

            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }


    @Override
    public Book getBookByIsbn(String isbn) {
        try {
            String sql = "SELECT * FROM BOOKS WHERE isbn=?";
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, isbn);

            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                return new Book(rs.getString("title"),
                        rs.getString("subtitle"),
                        rs.getString("author"),
                        rs.getString("isbn"),
                        rs.getInt("issued_in"));
            } else {
                throw new BookNotFoundException("The Book is not found in DB");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public Book getBookById(Integer id) {
        try {
            String sql = "SELECT * FROM BOOKS WHERE id=?";
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setInt(1, id);

            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                return new Book(rs.getString("title"),
                        rs.getString("subtitle"),
                        rs.getString("author"),
                        rs.getString("isbn"),
                        rs.getInt("issued_in"));
            } else {
                throw new BookNotFoundException("The Book is not found in DB");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Book> getAllBooks() {
        List<Book> books = new ArrayList<>();
        try {
            ResultSet rs = StatementInit.getConnection().createStatement().executeQuery("SELECT * FROM BOOKS");
            while (rs.next()) {
                books.add(new Book(rs.getString("title"),
                                rs.getString("subtitle"),
                                rs.getString("author"),
                                rs.getString("isbn"),
                                rs.getInt("issued_in")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return books;
    }

    @Override
    public void saveBook(Book book) {
        String sql = "INSERT INTO BOOKS (title,subtitle,author,isbn,issued_in) VALUES (?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getSubtitle());
            preparedStatement.setString(3, book.getAuthor());
            preparedStatement.setString(4, book.getIsbn());
            preparedStatement.setInt(5,book.getIssued_in());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
    @Override
    public void deleteBookByTitle(String title){
        String sql = "DELETE FROM BOOKS WHERE title=?";
        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, title);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void deleteBookById(Integer id) {
        String sql = "DELETE FROM BOOKS WHERE id=?";

        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setInt(1, id);

            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    @Override
    public void deleteBookByIsbn(String isbn) {
        String sql = "DELETE FROM BOOKS WHERE isbn=?";

        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, isbn);

            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void updateBook(Book book, String findBook) {
        String sql = "UPDATE BOOKS SET title=?, subtitle=?, author=?, isbn=?,issued_in=? WHERE title=?";

        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, book.getTitle());
            preparedStatement.setString(2, book.getSubtitle());
            preparedStatement.setString(3, book.getAuthor());
            preparedStatement.setString(4, book.getIsbn());
            preparedStatement.setInt(5,book.getIssued_in());
            preparedStatement.setString(6,findBook);
            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
    @Override
    public void updateIssued(Book book, Integer issued, Integer year,String isbn) {
        String sql = "UPDATE BOOKS SET issued_in=? WHERE issued_in=? AND isbn=?";

        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);

            preparedStatement.setInt(1,issued);
            preparedStatement.setInt(2,year);
            preparedStatement.setString(3,isbn);
            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }
}
